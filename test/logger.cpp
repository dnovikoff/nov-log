#define BOOST_TEST_MODULE main

#include <iostream>
#include <list>

#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <nov/log/logger.hpp>
#include <nov/log/default_logger.hpp>

using namespace Nov::Log;

namespace {

class TestMessage {
public:
	TestMessage(const LogLevel l, const std::string& m): level(l), message(m) {
	}

	void print() {
		std::cout << "[Level=" << level << "] " << message << std::endl;
	}
	bool operator==(const TestMessage& rhs) const {
		if (level != rhs.level) {
			return false;
		}
		return message == rhs.message;
	}

	bool operator!=(const TestMessage& rhs) const {
		return !operator ==(rhs);
	}

	LogLevel level;
	std::string message;
};

typedef std::list<TestMessage> MessageList;

void printMessages(const MessageList& mlist) {
	size_t i = 0;
	for(auto m: mlist) {
		std::cout << "[" << i++ << "]";
		m.print();
	}
}

bool compareMessages(const MessageList& lhs, const MessageList& rhs) {
	if(lhs.size() != rhs.size()) return false;
	return std::equal(rhs.begin(), rhs.end(), lhs.begin());
}

void printDiff(const MessageList& messages, const MessageList& ethalon) {
	std::cout << "Collections are not equal" << std::endl;
	std::cout << "=== Messages from test are:" << std::endl;
	printMessages( messages );
	std::cout << "=== Ethalon messages are:" << std::endl;
	printMessages( ethalon  );
}

bool compareAndPrint(const MessageList& messages, const MessageList& ethalon) {
	bool r = compareMessages(messages, ethalon);
	if(!r) printDiff( messages, ethalon );
	return r;
}

class TestLogger: public Logger {
public:
	TestLogger(const LogLevel th = defaultThreshold()): Logger(th) {
	}
	const MessageList& getMessages() const {
		return messages;
	}
	void clear() {
		messages.clear();
	}

private:
	void doWrite(const LogLevel level, const std::string& message) override {
		messages.push_back(TestMessage(level, message));
	}

	MessageList messages;
};

int throwException() {
	throw std::runtime_error("int throwException()");
}

} // namespace

BOOST_AUTO_TEST_CASE( printOneMessage ) {
	TestLogger logger;
	logger << "Hello!";
	const MessageList et{
		{LogLevel::INFO, "Hello!"},
	};
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( printTwoMessagesOnDifferentLogLevels ) {
	TestLogger logger;
	logger.info() << "Hello!";
	logger.error() << "World!";

	const MessageList et{
		{LogLevel::INFO, "Hello!"},
		{LogLevel::ERROR, "World!"},
	};
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( exceptionWhileConstructingMessage ) {
	TestLogger logger;
	BOOST_CHECK_THROW( logger.info() << "Hello " << throwException() << " World", std::runtime_error );
	MessageList et;
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
	BOOST_CHECK_THROW( logger.info() << throwException(), std::runtime_error );
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( loggingFromCatch ) {
	TestLogger logger;
	try {
		throw std::runtime_error("test");
	} catch(const std::runtime_error& x) {
		TestLogger logger;
		logger.write(LogLevel::INFO, "Hello");

		const MessageList et{
			{LogLevel::INFO, "Hello"},
		};
		BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
	}
}

BOOST_AUTO_TEST_CASE( direcTWrite ) {
	TestLogger logger;
	logger.write(LogLevel::INFO, "Hello");
	logger.write(LogLevel::ERROR, "World");

	const MessageList et{
		{LogLevel::INFO, "Hello"},
		{LogLevel::ERROR, "World"},
	};
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( defaultLogThreshold ) {
	TestLogger logger;

	BOOST_CHECK( logger.checkLevel(LogLevel::FATAL) );
	BOOST_CHECK( logger.checkLevel(LogLevel::ERROR) );
	BOOST_CHECK( logger.checkLevel(LogLevel::WARNING) );
	BOOST_CHECK( logger.checkLevel(LogLevel::INFO) );

	BOOST_CHECK( !logger.checkLevel(LogLevel::DEBUG) );
	BOOST_CHECK( !logger.checkLevel(LogLevel::TRACE) );
}

BOOST_AUTO_TEST_CASE( nonDefaultThreshold ) {
	TestLogger logger(LogLevel::FATAL);

	BOOST_CHECK( logger.checkLevel(LogLevel::FATAL) );

	BOOST_CHECK( !logger.checkLevel(LogLevel::ERROR) );
	BOOST_CHECK( !logger.checkLevel(LogLevel::WARNING) );
	BOOST_CHECK( !logger.checkLevel(LogLevel::INFO) );
	BOOST_CHECK( !logger.checkLevel(LogLevel::DEBUG) );
	BOOST_CHECK( !logger.checkLevel(LogLevel::TRACE) );
}

BOOST_AUTO_TEST_CASE( thresholdMessages ) {
	TestLogger logger(LogLevel::WARNING);

	logger.error()   << "error";
	logger.info()    << "info";
	logger.warning() << "warning";
	logger.trace()   << "trace";
	logger.debug()   << "debug";
	logger.fatal()   << "fatal";

	const MessageList et{
		{LogLevel::ERROR, "error"},
		{LogLevel::WARNING, "warning"},
		{LogLevel::FATAL, "fatal"},
	};
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( defaulThresholdMessages ) {
	TestLogger logger;
	logger.error()   << "error";
	logger.info()    << "info";
	logger.warning() << "warning";
	logger.trace()   << "trace";
	logger.debug()   << "debug";
	logger.fatal()   << "fatal";

	const MessageList et{
		{LogLevel::ERROR, "error"},
		{LogLevel::INFO, "info"},
		{LogLevel::WARNING, "warning"},
		{LogLevel::FATAL, "fatal"},
	};
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( noThreshold ) {
	TestLogger logger(LogLevel::TRACE);
	logger.error()   << "error";
	logger.info()    << "info";
	logger.warning() << "warning";
	logger.trace()   << "trace";
	logger.debug()   << "debug";
	logger.fatal()   << "fatal";

	const MessageList et{
		{LogLevel::ERROR, "error"},
		{LogLevel::INFO, "info"},
		{LogLevel::WARNING, "warning"},
		{LogLevel::TRACE, "trace"},
		{LogLevel::DEBUG, "debug"},
		{LogLevel::FATAL, "fatal"},
	};
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( outstreamDifferentTypes ) {
	TestLogger logger;
	logger << "Hello " << 123 << " World " << std::string("std::string");

	const MessageList et{
		{LogLevel::INFO, "Hello 123 World std::string"}
	};
	BOOST_CHECK( compareAndPrint( logger.getMessages(), et) );
}

BOOST_AUTO_TEST_CASE( defaultLogger ) {
	TestLogger backend;
	DefaultLogger d(backend, "Test");

	d.info() << "Hello!";

	const auto& messages = backend.getMessages();
	BOOST_REQUIRE_EQUAL(1u, messages.size());
	const auto& message = messages.back();
	BOOST_CHECK(LogLevel::INFO == message.level);
	std::cout << message.message << std::endl;
	BOOST_CHECK(boost::ends_with(message.message, "Test Tmain INF] Hello!"));
}

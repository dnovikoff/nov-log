#ifndef NOV_LOG_MESSAGE_HPP_
#define NOV_LOG_MESSAGE_HPP_

#include <sstream>
#include <memory>

#include <nov/log/log_level.hpp>

namespace Nov {
namespace Log {

class Logger;

class Message {
	Logger* logger;
	LogLevel logLevel;
	// no move constructor at ostringstream bug
	std::unique_ptr<std::ostringstream> message;

	void write(const char* data, const size_t size);

	friend class Logger;
	Message(Logger& l, LogLevel level);

	bool shouldBeLogged() const;

public:
	Message(Message&& other);
	Message& operator=(Message&& other);

	template<typename T>
	Message operator<<(const T& data) {
		// Optimization. If the level check fails - nothing will be done.
		if(shouldBeLogged()) {
			(*message) << data;
		}
		// Return with move constructor
		return std::move(*this);
	}
	~Message();

private:
	Message(const Message&) = delete;
	Message& operator=(const Message&) = delete;
};

} // namespace Log
} // namespace Nov

#endif // NOV_LOG_MESSAGE_HPP_

#include "default_logger.hpp"

namespace Nov {
namespace Log {

DefaultLogger::DefaultLogger(Logger& backend, const std::string& name): backend_(backend), decorator_(name, backend_) {
	decorator_.setThreshold(Nov::Log::LogLevel::ALL);

	// Logger threshold should be maximum
	// All messages will be filtered by DefaulLogger
	backend_.setThreshold(Nov::Log::LogLevel::ALL);
	decorator_.setThreshold(Nov::Log::LogLevel::ALL);
	setThreshold(Nov::Log::LogLevel::ALL);
}

void DefaultLogger::doWrite(const Nov::Log::LogLevel level, const std::string& message) {
	decorator_.write(level, message);
}

} // namespace Log
} // namespace Nov

#include <stdexcept>
#include <memory>

#include "logger.hpp"
#include "message.hpp"

namespace Nov {
namespace Log {

Message::Message(Message&&) = default;

Message& Message::operator=(Message&&) = default;

bool Message::shouldBeLogged() const {
	return logger->checkLevel(logLevel);
}

Message::Message(Logger& l, LogLevel level): logger(&l), logLevel(level), message(new std::ostringstream) {
}

void Message::write(const char* data, const size_t size) {
	message->write(data, size);
}

Message::~Message() {
	// Exception thrown, while constructing log message
	// The message is not defined for temp objects (moved)
	if(!message || !shouldBeLogged() || std::uncaught_exception()) {
		return;
	}

	// Message construction is over. We can now pass the message to logger
	logger->write(logLevel, message->str());
}

} // namespace Log
} // namespace Nov

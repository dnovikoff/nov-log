#ifndef NOV_LOG_LOGGER_STANDARD_FORMAT_DECORATOR_HPP_
#define NOV_LOG_LOGGER_STANDARD_FORMAT_DECORATOR_HPP_

#include <nov/log/named_logger.hpp>

namespace Nov {
namespace Log {

/**
 * Decorator for loggin in standard format
 * [data+time module_name thread_id] message
 */
class LoggerStandardFormatDecorator: public NamedLogger {
public:
	explicit LoggerStandardFormatDecorator(const std::string& name, Logger& logger);

private:
	void doWrite(const LogLevel level, const std::string& message) override;

	Logger& baseLogger;
};

} // namespace Log
} // namespace Nov

#endif // NOV_LOG_LOGGER_STANDARD_FORMAT_DECORATOR_HPP_

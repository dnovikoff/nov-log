#ifndef NOV_LOG_LOG_LEVEL_HPP_
#define NOV_LOG_LOG_LEVEL_HPP_

#include <ostream>

namespace Nov {
namespace Log {

enum class LogLevel {
	FATAL=1,
	ERROR,
	WARNING,
	INFO,
	DEBUG,
	TRACE,
	ALL=TRACE
};

/**
 * Get human readable name of log level
 */
extern const char* logLevelName(const LogLevel code);

} // namespace Log
} // namespace Nov

extern std::ostream& operator<<(std::ostream& out, const Nov::Log::LogLevel l);

#endif // NOV_LOG_LOG_LEVEL_HPP_

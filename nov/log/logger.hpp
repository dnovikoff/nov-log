#ifndef NOV_LOG_LOGGER_HPP_
#define NOV_LOG_LOGGER_HPP_

#include <nov/log/message.hpp>
#include <nov/log/log_level.hpp>

namespace Nov {
namespace Log {

/**
 * Abstract class for creating specific loggers
 * Does know nothing, about where and how the messages will be written
 * Provides threshold abilities to filter messages and shugar to easily log them
 */
class Logger {
public:
	/**
	 * @param threshold the last level that will be logged
	 */
	explicit Logger(const LogLevel th = defaultThreshold()):threshold(th) {
	}
	virtual ~Logger() = 0;

	void setThreshold(const LogLevel th) {
		threshold = th;
	}
	/**
	 * Some messages could be heavy to construct or
	 * present in some perfomance code
	 * in this case you can use the following construction:
	 * if(logger.checkLevel(LogLevel::DEBUG)) {
	 *   logger.debug() << "Your message";
	 * }
	 *
	 * @param level to check against threshold
	 */
	bool checkLevel(const LogLevel level) const {
		return level <= threshold;
	}

	void write(const LogLevel level, const std::string& message) {
		if (!checkLevel(level)) return;
		doWrite(level, message);
	}

	/**
	 * That means, that default logging level is info
	 */
	Message log(const LogLevel level = LogLevel::INFO);

	/**
	 * Shugar for logging into different levels
	 */
	//@{
	inline Message fatal()   { return log( LogLevel::FATAL   ); }
	inline Message error()   { return log( LogLevel::ERROR   ); }
	inline Message warning() { return log( LogLevel::WARNING ); }
	inline Message info()    { return log( LogLevel::INFO    ); }
	inline Message debug()   { return log( LogLevel::DEBUG   ); }
	inline Message trace()   { return log( LogLevel::TRACE   ); }
	//@}

	template<typename T>
	Message operator<<(const T& x) {
		return log() << x;
	}

protected:
	static constexpr LogLevel defaultThreshold() {
		return LogLevel::INFO;
	}

private:
	/**
	 * The only function, that should be overriden
	 */
	virtual void doWrite(const LogLevel level, const std::string& message) = 0;

	// non copyable
	Logger(const Logger&) = delete;
	Logger& operator=(const Logger&) = delete;

	LogLevel threshold;
};

} // namespace Log
} // namespace Nov

#endif // NOV_LOG_LOGGER_HPP_

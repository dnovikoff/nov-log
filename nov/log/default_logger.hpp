#ifndef NOV_LOG_DEFAULT_LOGGER_HPP_
#define NOV_LOG_DEFAULT_LOGGER_HPP_

#include <nov/log/logger.hpp>
#include <nov/log/logger_standard_format_decorator.hpp>

namespace Nov {
namespace Log {

class DefaultLogger: public Logger {
public:
	explicit DefaultLogger(Logger& backend, const std::string& name);
	void doWrite(const Nov::Log::LogLevel level, const std::string& message) override;

private:
	DefaultLogger(const DefaultLogger&) = delete;
	DefaultLogger& operator=(const DefaultLogger&) = delete;

	Logger& backend_;
	Nov::Log::LoggerStandardFormatDecorator decorator_;
};

} // namespace Log
} // namespace Nov

#endif // NOV_LOG_DEFAULT_LOGGER_HPP_

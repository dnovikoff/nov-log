#include <sstream>
#include <stdexcept>
#include "log_level.hpp"

namespace Nov {
namespace Log {

const char* logLevelName(const LogLevel code) {
	switch(code) {
	case(LogLevel::FATAL):   return "FTL";
	case(LogLevel::ERROR):   return "ERR";
	case(LogLevel::WARNING): return "WRN";
	case(LogLevel::INFO):    return "INF";
	case(LogLevel::DEBUG):   return "DBG";
	case(LogLevel::TRACE):   return "TRC";
	}

	std::ostringstream oss;
	oss << "Unknown logging level " << static_cast<int>(code);
	throw std::logic_error(oss.str());
}

} // namespace Log
} // namespace Nov

std::ostream& operator<<(std::ostream& out, const Nov::Log::LogLevel l) {
	out << Nov::Log::logLevelName(l);
	return out;
}

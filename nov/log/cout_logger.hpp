#ifndef NOV_LOG_COUT_LOGGER_HPP_
#define NOV_LOG_COUT_LOGGER_HPP_

#include <nov/log/logger.hpp>

namespace Nov {
namespace Log {

/**
 * Writes to std::cout
 * Thread safe (mutex locking inside)
 */
class CoutLogger: public Logger {
	void doWrite(const LogLevel level, const std::string& msg) override;
};

} // namespace Log
} // namespace Nov

#endif // NOV_LOG_COUT_LOGGER_HPP_

#include <iostream>
#include <mutex>

#include "cout_logger.hpp"

namespace Nov {
namespace Log {

void CoutLogger::doWrite(const LogLevel, const std::string& msg) {
	static std::mutex writeMutex;
	std::lock_guard<std::mutex> l(writeMutex);
	std::cout << msg << std::endl;
}

} // namespace Log
} // namespace Nov

#include "logger.hpp"

namespace Nov {
namespace Log {

Logger::~Logger() {
}

Message Logger::log(const LogLevel level) {
	return Message(*this, level);
}

} // namespace Log
} // namespace Nov

#include "logger_standard_format_decorator.hpp"
#include <thread>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace Nov {
namespace Log {

LoggerStandardFormatDecorator::LoggerStandardFormatDecorator(const std::string& name, Logger& logger):NamedLogger(name), baseLogger(logger) {
}

void LoggerStandardFormatDecorator::doWrite(const LogLevel level, const std::string& message) {
	auto m = (baseLogger.log(level) << '[' << boost::posix_time::second_clock::local_time() << " " << getName() << " T");
	auto threadId = std::this_thread::get_id();
	if (threadId == std::thread::id()) {
		m = (m << "main");
	} else {
		m = (m << threadId);
	}
	m << " " << level << "] " << message;
}

} // namespace Log
} // namespace Nov

#ifndef NOV_LOG_NAMED_LOGGER_HPP_
#define NOV_LOG_NAMED_LOGGER_HPP_

#include <nov/log/logger.hpp>

namespace Nov {
namespace Log {

class NamedLogger: public Logger {
public:
	explicit NamedLogger(const std::string& n): name(n) {
	}

protected:
	const std::string& getName() const {
		return name;
	}

private:
	const std::string name;
};

} // namespace Log
} // namespace Nov

#endif // NOV_LOG_NAMED_LOGGER_HPP_
